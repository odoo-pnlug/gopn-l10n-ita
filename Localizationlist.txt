NOME REPOSITORY	OCA		NOME MODULO
-----------------------------------------------------

l10n-italy(tutti i moduli 10.0)	account_invoice_report_ddt_group
				account_vat_period_end_statement
				l10n_it_abicab
				l10n_it_account
				l10n_it_account_tax_kind
				l10n_it_ateco
				l10n_it_base_location_geonames_import
				l10n_it_codici_carica
				l10n_it_ddt
				l10n_it_esigibilita_iva
				l10n_it_fatturapa
				l10n_it_fatturapa_out
				l10n_it_fiscalcode
				l10n_it_ipa
				l10n_it_pec
				l10n_it_rea
				l10n_it_ricevute_bancarie
				l10n_it_split_payment
				l10n_it_vat_registries
				l10n_it_withholding_tax
				l10n_it_withholding_tax_payment

***DIPENDENZE DI L10N-ITALY***

account-payment			account_due_list

account-financial-reporting	account_tax_balance

partner-contact			base_location
				base_location_geonames_import

server-tools			date_range

stock-logistics-workflow	stock_picking_package_preparation
				stock_picking_package_preparation_line


***ALTRI MODULI UTILI***

sale-reporting			sale_proforma_report

account-invoicing		account_invoice_force_number

REPO STANDARD ODOO		account_cancel





